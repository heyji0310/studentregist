package com.anhj.dbtest2.repository;

import com.anhj.dbtest2.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
