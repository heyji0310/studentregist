package com.anhj.dbtest2.controller;

import com.anhj.dbtest2.entity.Student;
import com.anhj.dbtest2.model.StudentItem;
import com.anhj.dbtest2.model.StudentRequest;
import com.anhj.dbtest2.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;

    @PostMapping("/new")
    public String setStudent(@RequestBody StudentRequest request) {
        studentService.setStudent(request.getName(), request.getBirthday(), request.getGender());
        return "OK";
    }

    @GetMapping("/all")
    public List<StudentItem> getStudents() {
        return studentService.getStudents();
    }

    @PutMapping("/{id}") // {id} 는 변수
    public String putStudent(@PathVariable long id, @RequestBody StudentRequest request) {
        studentService.putStudent(id, request.getName(), request.getBirthday(), request.getGender());
        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delStudent(@PathVariable long id) {
        studentService.delStudent(id);
        return "OK";
    }
}
