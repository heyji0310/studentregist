package com.anhj.dbtest2.model;

import com.anhj.dbtest2.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Getter
@Setter
public class StudentRequest {
    private String name;
    private LocalDate birthday;
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
}
