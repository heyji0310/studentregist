package com.anhj.dbtest2.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class StudentItem {
    private Long id;
    private String name;
    private LocalDate birthday;
    private String gender;
}
