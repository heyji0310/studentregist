package com.anhj.dbtest2.service;

import com.anhj.dbtest2.entity.Student;
import com.anhj.dbtest2.enums.Gender;
import com.anhj.dbtest2.model.StudentItem;
import com.anhj.dbtest2.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService { // Create 부분
    private final StudentRepository studentRepository;

    public void setStudent(String name, LocalDate birthday, Gender gender) {
        Student student = new Student();
        student.setName(name);
        student.setBirthday(birthday);
        student.setGender(gender);

        studentRepository.save(student); // student entity 전용 배달부
    }

    public List<StudentItem> getStudents() { // Read 부분
        List<StudentItem> result = new LinkedList<>();

        List<Student> students = studentRepository.findAll(); // Read
        for (Student student : students) {
            StudentItem newItem = new StudentItem();
            newItem.setId(student.getId());
            newItem.setName(student.getName());
            newItem.setBirthday(student.getBirthday());
            newItem.setGender(student.getGender().getName()); // student gender의 name("여자", "남자")를 가져와

            result.add(newItem); // 값을 newItem 박스에 넣어
        }

        return result;
    }

    public void putStudent(long id, String name, LocalDate birthday, Gender gender) { // update 부분
        Student student = studentRepository.findById(id).orElseThrow();
                                          // studentRepository에서 id를 가지고 오는데, 오류가 생기면 던져
        student.setName(name); // student에 name, birthday, gender를 넣어줘
        student.setBirthday(birthday);
        student.setGender(gender);
        studentRepository.save(student); // 넣고 저장해
    }

    public void delStudent(long id) { // delete
        studentRepository.deleteById(id);
    }
}
